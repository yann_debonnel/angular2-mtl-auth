## Synopsis

Cette librairie à pour but de faciliter l'implémentation de système d'authentification OpenID pour les applications Angular2 de la Ville de Montréal.

## Fonctionnalités

- MtlAuthenticationGuard: en utilisant ce guard, les développeurs peuvent spécifier que seuls les usagers authentifiés peuvent accéder à certaines routes. Les usagers non-authentifiés seront redirigés à l'écran d'authentification.
- Silent-Refresh: Les jetons d'accès seront rafraichis automatiquement par la librairie grâce à un iframe invisible à certaines intervalles de temps.
- Les requêtes HTTP seront interceptées par la librairie et l'entête d'authorization sera ajoutée automatiquement.
- Si une requête HTTP est faite alors que les jetons d'accès sont expirés, ceux-si vont se rafraîchir silencieusement avant que la requête procède.
- Si un navigation interne est faite alors que les jetons d'accès sont expirés, ceux-si vont se rafraîchir silencieusement avant que la navigation procède.

## Installation

```
cd <racine de votre projet>

TODO: add MTL NEXUS

npm install -s angula2-mtl-auth
```

## Utilisation

Ajouter le modules nécessaires au module principal de votre application
```
...
import { MtlAuthenticationModule, OAuth2OidcCertProviderModule } from 'angular2-mtl-auth';
...

@NgModule({
  ...
  imports: [
    OAuth2OidcCertProviderModule.forRoot(),
    MtlAuthenticationModule.forRoot(environment.authentificationConfig),
  ]
  ...
})
export class AppModule { }
```

Votre composant principal doit hériter de la classe `MtlAuthenticationSecureComponent`
```
import {MtlAuthenticationSecureComponent} from 'angular2-mtl-auth';
...
export class AppComponent extends MtlAuthenticationSecureComponent {

  public errorMessage: Message;
  public showHeader = false;

  constructor(protected injector: Injector) {
    super(injector);
...
  }
}
```

Définir `MtlAuthenticationGuard` comme le 'Route Guard' pour les routes demandant de l'authentification
```
import { MtlAuthenticationGuard } from 'angular2-mtl-auth';

const routes: Routes = [
  {
    path: '',
    component: MyComponent,
    canActivate: [MtlAuthenticationGuard]
  }
];

export const myRouting = RouterModule.forChild(routes);
```



## Contributors

 - Yann Debonnel [yann.debonnel@ville.montreal.qc.ca](mailto:yann.debonnel@ville.montreal.qc.ca)

## License

MIT