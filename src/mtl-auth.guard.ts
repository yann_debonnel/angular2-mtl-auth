import { Injectable, Inject, Injector } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { MtlAuthenticationProvider } from './mtl-auth-provider.interface';
import {
  AUTHENTICATION_ACTIVATION,
  AUTHENTICATION_POST_LOGOUT_REDIRECT_URL,
  AUTHENTICATION_REDIRECT_URL,
  AUTHENTICATION_UNAUTHORIZED_ROUTE,
  AUTHENTICATION_LOGIN_URL
} from './angular2-mtl-auth.module';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/of';

@Injectable()
export class MtlAuthenticationGuard implements CanActivate, CanActivateChild {
  private loginUrl: string;
  private whiteList: string[];
  protected authenticationActivated: boolean;
  protected authenticationService: MtlAuthenticationProvider;
  protected router: Router;
  protected canActivateObs: Subject<boolean> = new Subject<boolean>();

  constructor(
    protected injector: Injector,
  ) {
    console.log('SecureComponent - constructor');
    this.authenticationActivated = this.injector.get(AUTHENTICATION_ACTIVATION);
    this.loginUrl = this.injector.get(AUTHENTICATION_LOGIN_URL);
    this.whiteList = [
      this.injector.get(AUTHENTICATION_LOGIN_URL),
      this.injector.get(AUTHENTICATION_POST_LOGOUT_REDIRECT_URL),
      this.injector.get(AUTHENTICATION_REDIRECT_URL),
      this.injector.get(AUTHENTICATION_UNAUTHORIZED_ROUTE),
      this.injector.get(AUTHENTICATION_UNAUTHORIZED_ROUTE),
    ];
    if (this.authenticationActivated) {
      this.authenticationService = this.injector.get(MtlAuthenticationProvider);
      this.router = this.injector.get(Router);
      this.authenticationService.setup();
    }
  }

  canActivate(next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    const canActivateSubj = new Subject<boolean>();
    if (this.authenticationService.getAccessToken() && this.authenticationService.isAccessTokenExpired()) {
      // if the token is expired, perform a silent refresh before checking if canActivate
      return this.authenticationService.silentRefresh().flatMap((success: boolean) => {
        return this.__canActivate(next, state);
      });
    } else {
      return this.__canActivate(next, state);
    }
  }


  __canActivate(next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    if (this.authenticationActivated) {
      return this.authenticationService.getIsAuthorized().flatMap((isAuthorized: boolean) => {
        if (!isAuthorized) {
           this.router.navigateByUrl('login');
         }
        return Observable.of(isAuthorized);
      });
    }
    return Observable.of(false);
  }


  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(childRoute, state);
  }

}
