import { Component, OnInit, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { MtlAuthenticationProvider } from './mtl-auth-provider.interface';
import { AUTHENTICATION_ACTIVATION } from './angular2-mtl-auth.module';

@Component({
  selector: 'mtl-auth-secure',
  template: ``
})
export class MtlAuthenticationSecureComponent {

  public isAuthorized: false;

  protected authenticationActivated: boolean;
  protected authenticationService: MtlAuthenticationProvider;
  protected router: Router;


  constructor(
    protected injector: Injector,
  ) {
    console.log('MtlAuthSecureComponent - ctor');
    this.authenticationActivated = this.injector.get(AUTHENTICATION_ACTIVATION);
    if (this.authenticationActivated) {
      this.authenticationService = this.injector.get(MtlAuthenticationProvider);
      this.router = this.injector.get(Router);
      this.authenticationService.setup();
    }
  }

  logout() {
    this.authenticationService.logout();
  }

}
