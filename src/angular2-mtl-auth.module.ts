import { NgModule, ModuleWithProviders, InjectionToken } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {
  MtlAuthLibraryConfig
} from './mtl-auth-lib-config';
import { MtlAuthenticationGuard } from './mtl-auth.guard';
import { MtlAuthenticationHttpInterceptor } from './mtl-auth-http.interceptor';
import { OAuth2OidcCertProviderModule } from './angular-oauth2-oidc-provider/angular-oauth2-oidc-provider.module';
import { MtlAuthenticationSecureComponent } from './mtl-auth-secure.component';

// tslint:disable-next-line:max-line-length
export const AUTHENTICATION_ACTIVATION: InjectionToken<boolean> = new InjectionToken<boolean>('authentication_activation');
export const AUTHENTICATION_SERVER_URL: InjectionToken<string> = new InjectionToken<string>('authentication_server_url');
export const AUTHENTICATION_CLIENT_ID: InjectionToken<string> = new InjectionToken<string>('authentication_client_id');
export const AUTHENTICATION_REDIRECT_URL: InjectionToken<string> = new InjectionToken<string>('authentication_redirect_url');
export const AUTHENTICATION_SILENT_REFRESH_URL: InjectionToken<string> = new InjectionToken<string>('silent_refresh_url');
export const AUTHENTICATION_CUSTOM_QUERY_PARAMS: InjectionToken<any> = new InjectionToken<any>('authentication_custom_query_params');
export const AUTHENTICATION_LOGIN_URL: InjectionToken<string> = new InjectionToken<string>('authentication_login_url');
// tslint:disable-next-line:max-line-length
export const AUTHENTICATION_POST_LOGOUT_REDIRECT_URL: InjectionToken<string> = new InjectionToken<string>('authentication_post_logout_redirect_url');
export const AUTHENTICATION_SCOPE: InjectionToken<string> = new InjectionToken<string>('authentication_scope');
export const AUTHENTICATION_AUTO_RENEW: InjectionToken<boolean> = new InjectionToken<boolean>('authentication_auto_renew');
export const AUTHENTICATION_STARTUP_ROUTE: InjectionToken<string> = new InjectionToken<string>('authentication_startup_route');
export const AUTHENTICATION_FORBIDDEN_ROUTE: InjectionToken<string> = new InjectionToken<string>('authentication_forbidden_route');
export const AUTHENTICATION_UNAUTHORIZED_ROUTE: InjectionToken<string> = new InjectionToken<string>('authentication_unauthorized_route');

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    OAuth2OidcCertProviderModule,
  ],
  declarations: [
    MtlAuthenticationSecureComponent
  ],
  exports: [
    OAuth2OidcCertProviderModule,
    MtlAuthenticationSecureComponent
  ],
})
export class MtlAuthenticationModule {
  static forRoot(config: MtlAuthLibraryConfig): ModuleWithProviders {
    return {
      ngModule: MtlAuthenticationModule,
      providers: [
        {
          provide: AUTHENTICATION_SERVER_URL,
          useValue: config.authServerUrl
        },
        {
          provide: AUTHENTICATION_LOGIN_URL,
          useValue: config.loginUrl
        },
        {
          provide: AUTHENTICATION_ACTIVATION,
          useValue: config.activation !== undefined ? config.activation : true
        },
        {
          provide: AUTHENTICATION_CLIENT_ID,
          useValue: config.clientId
        },
        {
          provide: AUTHENTICATION_REDIRECT_URL,
          useValue: config.postLoginRedirectUrl
        },
        {
          provide: AUTHENTICATION_POST_LOGOUT_REDIRECT_URL,
          useValue: config.postLogoutRedirectUrl
        },
        {
          provide: AUTHENTICATION_SCOPE,
          useValue: config.scope
        },
        {
          provide: AUTHENTICATION_AUTO_RENEW,
          useValue: config.silentRefreshEnabled
        },
        {
          provide: AUTHENTICATION_STARTUP_ROUTE,
          useValue: config.startupUrl
        },
        {
          provide: AUTHENTICATION_FORBIDDEN_ROUTE,
          useValue: config.forbiddenUrl
        },
        {
          provide: AUTHENTICATION_UNAUTHORIZED_ROUTE,
          useValue: config.unauthorizedUrl
        },
        {
          provide: AUTHENTICATION_CUSTOM_QUERY_PARAMS,
          useValue: config.customQueryParams
        },
        {
          provide: AUTHENTICATION_SILENT_REFRESH_URL,
          useValue: config.silentRefreshUrl
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: MtlAuthenticationHttpInterceptor,
          multi: true
        },
        MtlAuthenticationGuard
      ]
    };
  }
}
