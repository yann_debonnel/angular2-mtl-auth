export interface MtlAuthLibraryConfig {
  // is the authentication activated
  activation?: boolean;
  // the authentication server url
  authServerUrl: string;
  // map with additional query parameter that are appended to the request when initializing implicit flow.
  customQueryParams?: object;
  // the client id
  clientId: string;
  // the requested scope
  scope?: string;
  // is silent refresh enabled (refreshing tokens automatically before they expire)
  silentRefreshEnabled?: boolean;
  // the callback url for the silent refresh
  silentRefreshUrl?: string;
  // the app's login url
  loginUrl: string;
  // the url the authentication server should redirect to once a session is established
  postLoginRedirectUrl: string;
  // the url the authentication server should redirect to once a session is terminated
  postLogoutRedirectUrl: string;
  // the app's starting route once authorization is establisheds
  startupUrl?: string;
  // the app's forbidden route
  forbiddenUrl?: string;
  // the app's unauthorized route
  unauthorizedUrl?: string;
}
