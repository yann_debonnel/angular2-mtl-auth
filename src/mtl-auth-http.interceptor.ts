import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';

import { MtlAuthenticationProvider } from './mtl-auth-provider.interface';
import { AUTHENTICATION_ACTIVATION } from './angular2-mtl-auth.module';

@Injectable()
export class MtlAuthenticationHttpInterceptor implements HttpInterceptor {
  protected authenticationActivated: boolean;
  protected authenticationService: MtlAuthenticationProvider;

  constructor(protected injector: Injector) {
    this.authenticationActivated = this.injector.get(AUTHENTICATION_ACTIVATION);
    this.authenticationService = this.injector.get(MtlAuthenticationProvider);
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.authenticationService.getAccessToken() && this.authenticationService.isAccessTokenExpired()) {
      // preform silent refresh then handle request
      return this.authenticationService.silentRefresh().flatMap((success: boolean) => {
        return this.__intercept(request, next);
      });
    } else {
      return this.__intercept(request, next);
    }
  }

  __intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
    const token = this.authenticationService.getToken();
    request = request.clone({
      setHeaders: {
        'Content-Type': 'application/json'
      }
    });
    if (this.authenticationActivated && token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    }
    return next.handle(request);
  }

}
