import { Injectable, Inject } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { AuthConfig } from 'angular-oauth2-oidc';
import { JwksValidationHandler } from 'angular-oauth2-oidc';
import { MtlAuthenticationProvider } from '../mtl-auth-provider.interface';

import { Observable } from 'rxjs/Observable';
import { OAuthEvent, EventType } from 'angular-oauth2-oidc/events';
import { BehaviorSubject, Subject } from 'rxjs';
import {
  AUTHENTICATION_SERVER_URL,
  AUTHENTICATION_REDIRECT_URL,
  AUTHENTICATION_POST_LOGOUT_REDIRECT_URL,
  AUTHENTICATION_CUSTOM_QUERY_PARAMS,
  AUTHENTICATION_CLIENT_ID,
  AUTHENTICATION_SCOPE,
  AUTHENTICATION_STARTUP_ROUTE,
  AUTHENTICATION_FORBIDDEN_ROUTE,
  AUTHENTICATION_UNAUTHORIZED_ROUTE,
  AUTHENTICATION_SILENT_REFRESH_URL
} from '../angular2-mtl-auth.module';

@Injectable()
export class OAuth2OidcProviderService implements MtlAuthenticationProvider {

  constructor(private oauthService: OAuthService,
    @Inject(AUTHENTICATION_SERVER_URL) private serverUrl: string,
    @Inject(AUTHENTICATION_REDIRECT_URL) private redirectUrl: string,
    @Inject(AUTHENTICATION_POST_LOGOUT_REDIRECT_URL) private postLogoutRedirectUrl: string,
    @Inject(AUTHENTICATION_CUSTOM_QUERY_PARAMS) private customQueryParams: any,
    @Inject(AUTHENTICATION_CLIENT_ID) private clientId: string,
    @Inject(AUTHENTICATION_SCOPE) private scope: string,
    @Inject(AUTHENTICATION_STARTUP_ROUTE) private startupRoute: string,
    @Inject(AUTHENTICATION_FORBIDDEN_ROUTE) private forbiddenRoute: string,
    @Inject(AUTHENTICATION_UNAUTHORIZED_ROUTE) private unauthorizedRoute: string,
    @Inject(AUTHENTICATION_SILENT_REFRESH_URL) private silentRefreshUrl: string
  ) {
  }

  setup() {
    const authConfig: AuthConfig = {
      // Url of the Identity Provider
      issuer: this.serverUrl,
      loginUrl: this.serverUrl + '/oxauth/authorize',
      // URL of the SPA to redirect the user to after login
      redirectUri: this.redirectUrl,
      // URL of the SPA to redirect the user to after logout
      postLogoutRedirectUri: this.postLogoutRedirectUrl,
      // The SPA's id. The SPA is registerd with this id at the auth-server
      clientId: this.clientId,
      // set the scope for the permissions the client should request
      // The first three are defined by OIDC. The 4th is a usecase-specific one
      scope: this.scope,
      silentRefreshRedirectUri: this.silentRefreshUrl,
      timeoutFactor: 0.75,
      customQueryParams: this.customQueryParams
    };
    // const authConfig: AuthConfig = {
    //   // Url of the Identity Provider
    //   issuer: 'https://auth.dev.interne.montreal.ca',
    //   loginUrl: 'https://auth.dev.interne.montreal.ca/oxauth/authorize',
    //   // URL of the SPA to redirect the user to after login
    //   redirectUri: 'http://localhost:3333/authorize',
    //   // URL of the SPA to redirect the user to after logout
    //   postLogoutRedirectUri: 'http://localhost:3333/logout',
    //   // The SPA's id. The SPA is registerd with this id at the auth-server
    //   clientId: '@!4025.CA62.9BB6.16C5!0001!2212.0010!0008!CF92.1169.C3D5.51A0',
    //   // set the scope for the permissions the client should request
    //   // The first three are defined by OIDC. The 4th is a usecase-specific one
    //   scope:'openid profile user_name',
    //   silentRefreshRedirectUri: 'http://localhost:3333/silent-refresh.html',
    //   timeoutFactor: 0.75,
    //   customQueryParams: {
    //     ui_locales: 'fr',
    //     acr_values: 'basic_multi_auth_conf_emp'
    //   }
    // };
    this.oauthService.configure(authConfig);
    this.oauthService.setStorage(localStorage);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
    // Optional
    this.oauthService.setupAutomaticSilentRefresh();
  }

  login() {
    this.oauthService.initImplicitFlow();
  }

  logout() {
    this.oauthService.logOut();
  }

  loginCallBack() {
    this.oauthService.loadDiscoveryDocument().then((doc) => {
      this.oauthService.tryLogin().then(() => {
        location.href = '/'//this.startupRoute;
      });
    });
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
  }

  getIsAuthorized(): Observable<boolean> {
    return Observable.of(this.oauthService.hasValidAccessToken() && this.oauthService.hasValidIdToken());
  }

  getToken() {
    return this.oauthService.getAccessToken();
  }

  getPayload() {
    return this.oauthService.getIdentityClaims();
  }

  isAccessTokenExpired(): boolean {
    return Date.now() >= this.oauthService.getAccessTokenExpiration();
  }

  getAccessToken(): string {
    return this.oauthService.getAccessToken();
  }

  silentRefresh(): Observable<boolean> {
    const silentRefreshSubject = new Subject<boolean>();
    setTimeout(() => {
      this.oauthService.silentRefresh().then((evt: OAuthEvent) => {
        silentRefreshSubject.next(true);
        silentRefreshSubject.complete();
      }).catch((err: any) => {
        console.log('Silent refresh failed', err);
        silentRefreshSubject.next(false);
        silentRefreshSubject.complete();
      });
    }, 0);
    return silentRefreshSubject.asObservable();
  }

}
