import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OAuthModule } from 'angular-oauth2-oidc';

import { MtlAuthenticationProvider } from '../mtl-auth-provider.interface';
import { OAuth2OidcProviderService } from './angular-oauth2-oidc-provider.service';

@NgModule({
  imports: [
    CommonModule,
    OAuthModule.forRoot()
  ],
  declarations: [

  ],
  providers:[{
    provide: MtlAuthenticationProvider,
    useClass: OAuth2OidcProviderService
  }]
})
export class OAuth2OidcCertProviderModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: OAuth2OidcCertProviderModule,
      providers: [
        {
          provide: MtlAuthenticationProvider,
          useClass: OAuth2OidcProviderService
        }
      ]
    };
  }
}
