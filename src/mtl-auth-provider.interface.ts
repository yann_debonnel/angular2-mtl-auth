import { Observable } from 'rxjs/Observable';

export abstract class MtlAuthenticationProvider {

  constructor() { }

  abstract setup(): any;
  abstract login(): any;
  abstract logout(): any;
  abstract loginCallBack(): any;
  abstract getIsAuthorized(): Observable<boolean>;
  abstract getToken(): any;
  abstract getPayload(): any;
  abstract silentRefresh(): Observable<boolean>;
  abstract getAccessToken(): string;
  abstract isAccessTokenExpired(): boolean;
}


